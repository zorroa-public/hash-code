[![License][license-img]][license-url]
=
### hash-code

### api
* hashCode - same to `java` hashCode()

```js
var hash = require('hash-code');
var c = hash.hashCode('hash');
```

### License
MIT

[license-img]: http://img.shields.io/badge/license-MIT-green.svg?style=flat-square
[license-url]: http://opensource.org/licenses/MIT
