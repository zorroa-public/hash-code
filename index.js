'use strict';

exports.hashCode = function(str, optionalHashCode) {
  let hash = optionalHashCode ? optionalHashCode : 0;
  for (let i = 0, len = str.length; i < len; i++) {
    hash = ((hash << 5) - hash) + str.charCodeAt(i);
    hash = hash & hash;
  }
  return hash;
};
